#pragma once

#include <stdint.h>
#include <SDL/SDL.h>

// ====================================
#define DEF_V "0.0a"
#define DEF_T "WNGF test" DEF_V
#define DEF_W (1280)
#define DEF_H (720)

#define DEF_FOV (90.f)
#define DEF_TFPS (60)

#define WNGF_WHITE  (uint32_t)0xffffffff
#define WNGF_BLACK  (uint32_t)0x000000
#define WNGF_RED    (uint32_t)0xff0000
#define WNGF_GREEN  (uint32_t)0x00ff00
#define WNGF_BLUE   (uint32_t)0x0000ff

// Error messages and parts
#define WNGF_ALLOC_ERROR 	"Failed to allocate space for"
#define WNGF_ALLOC_ERROR_ID	(-2)

#define WNGF_BUFF_MAT4		"WNGF_mat4x4"
#define WNGF_BUFF_PTR_MAT4	"WNGF_mat4x4*"

#define WNGF_BUFF_TRI		"WNGF_triangle"
#define WNGF_BUFF_PTR_TRI	"WNGF_triangle*"

#define WNGF_BUFF_MESH		"WNGF_mesh"
#define WNGF_BUFF_PTR_MESH	"WNGF_mesh*"



#define WNGF_DEBUG
#define WNGF_COUNT_FPS
// #define WNGF_ROTATIONS_DISABLED

// ====================================

struct WNGF_vec2
{
	float x,y;
};
typedef struct WNGF_vec2 WNGF_vec2;

struct WNGF_vec3
{
    float x,y,z;
};
typedef struct WNGF_vec3 WNGF_vec3;

struct WNGF_vec4
{
	float x,y,z,w;
};
typedef struct WNGF_vec4 WNGF_vec4;

struct WNGF_mat4x4
{
	float mx[4][4];
};
typedef struct WNGF_mat4x4* WNGF_mat4x4_ptr;

struct WNGF_triangle
{
    WNGF_vec3 verts[3];
    WNGF_vec3 normals[3];
    WNGF_vec2 texcoords[3];
};
typedef struct WNGF_triangle* WNGF_triangle_ptr;

struct WNGF_mesh
{
    WNGF_triangle_ptr tris; // Array of structs
    uint_fast32_t tris_count;
};
typedef struct WNGF_mesh* WNGF_mesh_ptr;

struct WNGF_renderer
{
    SDL_Surface *final_surf; // if NULL, GE passes
    
    // Rendering-related
    WNGF_mat4x4_ptr projection_matx;
    uint16_t gf_w, gf_h;
    float gf_aspect, gf_fov, gf_znear, gf_zfar;

    SDL_Surface *render_texture; // Current texture
    uint32_t render_color;
    uint8_t draw_mode;
    //

    // Pointers to clear at end
    WNGF_mat4x4_ptr *buffers_matrix;
    size_t buffers_matrix_count;

    WNGF_triangle_ptr *buffers_triangle;
    size_t buffers_triangle_count;

    WNGF_mesh_ptr *buffers_mesh;
    size_t buffers_mesh_count;
    //
};

enum WNGF_DRAW_MODES
{
    WNGF_DRAW_WIREFRAME=0x02,
    WNGF_DRAW_FILLED=0x04,
    WNGF_DRAW_TEXTURED=0x16,
};

// ======== WNGF Functions ============
	// ------- Core functions ---------
	/* Configure and setup renderer
	 * This includes:
	 *	*	Creating and setting up projection matrix;
	 *	*	Creating and clearing buffers;
	 */
        void wngf_init_renderer(uint16_t width,                         // Renderer width
                                uint16_t height,                        // Renderer height
                                float fov,                              // Field of view
                                struct WNGF_renderer **target);         // Renderer
	
        void wngf_quit(struct WNGF_renderer *target); // Renderer

        void wngf_set_triangle_verts(	WNGF_triangle_ptr tri,			// Triangle (must exist)
                                        float x0,float y0,float z0,		// Vertex 0
                                        float x1,float y1,float z1,		// Vertex 1
                                        float x2,float y2,float z2);	// Vertex 2

        void wngf_set_triangle_norm(WNGF_triangle_ptr tri,
                                    float nx, float ny, float nz);

        void wngf_set_drawmode(struct WNGF_renderer *target, uint8_t mode);

        void wngf_set_color(struct WNGF_renderer *target, uint8_t r, uint8_t g, uint8_t b);

        void wngf_drawline( struct WNGF_renderer *target,           // Renderer
                            float x0, float y0,                     // Start of line
                            float x1, float y1);                    // End of line

        void wngf_drawtri(struct WNGF_renderer *target,             // Renderer
                          WNGF_triangle_ptr tri);                   // Triangle to draw

        void wngf_drawmesh( struct WNGF_renderer *target,           // Renderer
                            WNGF_mesh_ptr mesh,                     // Mesh to draw
                            float px, float py, float pz,           // Position (world)
                            float rx, float ry, float rz,           // Rotation (Euler)
                            WNGF_mat4x4_ptr mat_rot_x,              // Rotation matrix X
                            WNGF_mat4x4_ptr mat_rot_y,              // Rotation matrix Y
                            WNGF_mat4x4_ptr mat_rot_z,              // Rotation matrix Z
                            WNGF_triangle_ptr *tmp_tris);           // Temporary triangles [4]

        void wngf_drawtext(struct WNGF_renderer *target,            // Renderer
                           short px, short py,                // Position on screen
                           const char *txt,                         // Text
                           uint32_t color);                         // Color

        void wngf_clear_renderer(struct WNGF_renderer *target,      // Renderer
                                 uint32_t color);                   // Clear color
        // ---------------------------------------------------------------------
	
	// ------ Buffers functions ------- (Working with buffers and temporary data)
	WNGF_triangle_ptr wngf_create_buffer_triangle(struct WNGF_renderer *target);
	
	WNGF_mat4x4_ptr wngf_create_buffer_mat4x4(struct WNGF_renderer *target);
	
	WNGF_mesh_ptr wngf_create_buffer_mesh(struct WNGF_renderer *target,
										  uint_fast32_t triangle_count);
	// --------------------------------
	
	// -------- Math functions --------
    void wngf_matrix_mul_vec3(	WNGF_mat4x4_ptr m,                  // Matrix
                                WNGF_vec3 *v1,                      // Multiply vector
                                WNGF_vec3 *out);                    // Output vector

    void wngf_normalize_vec3(WNGF_vec3 *vec);
	// --------------------------------
	
	// ------ Helpful functions ------- (Mandatory, not necessary)
    WNGF_mesh_ptr wngf_init_cube(struct WNGF_renderer* target,      // Target renderer
                                 float a);                          // Size of side
	
    void wngf_set_matrix_rx(WNGF_mat4x4_ptr m,                      // Output matrix
                            float a);                               // Angle of rotation on axis X
	
    void wngf_set_matrix_ry(WNGF_mat4x4_ptr m,                      // Output matrix
                            float a);                               // Angle of rotation on axis Y
	
    void wngf_set_matrix_rz(WNGF_mat4x4_ptr m,                      // Output matrix
                            float a);                               // Angle of rotation on axis Z
	
	// --------------------------------
	
	// - Debug-only purpose functions -
	
    void wngf_debug_mesh_tris(const WNGF_mesh_ptr mesh);            // Mesh
	
	// --------------------------------
// ====================================
