#include "../include/wngf_core.h"
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_gfxPrimitives_font.h>
#include <time.h>

void wngf_init_renderer(uint16_t width, uint16_t height, float fov, struct WNGF_renderer **target)
{
    *target = malloc(sizeof(struct WNGF_renderer));

    (*target)->gf_w = width; (*target)->gf_h = height;
    (*target)->gf_aspect = (float)height / (float)width;
    (*target)->gf_fov = fov;

    (*target)->gf_znear = 0.1f; (*target)->gf_zfar = 1000.f;
    (*target)->render_color    = WNGF_WHITE;
    (*target)->render_texture  = NULL;

    // Nullify all buffers and their sizes
    (*target)->buffers_triangle    = NULL; (*target)->buffers_triangle_count	= 0;
    (*target)->buffers_matrix      = NULL; (*target)->buffers_matrix_count	= 0;
    (*target)->buffers_mesh        = NULL; (*target)->buffers_mesh_count		= 0;
    //

    // Init projection matrix
    (*target)->projection_matx = wngf_create_buffer_mat4x4(*target);

    float _fovtanrad = 1.f / tanf(fov * .5f / 180.f * (float)M_PI);

    (*target)->projection_matx->mx[0][0] = (*target)->gf_aspect * _fovtanrad;
    (*target)->projection_matx->mx[1][1] = _fovtanrad;
    (*target)->projection_matx->mx[2][2] = (*target)->gf_zfar /
                                        ((*target)->gf_zfar - (*target)->gf_znear);

    (*target)->projection_matx->mx[3][2] = (-(*target)->gf_zfar * (*target)->gf_znear) /
                                            ((*target)->gf_zfar - (*target)->gf_znear);

    (*target)->projection_matx->mx[2][3] = 1.f;
    (*target)->projection_matx->mx[3][3] = 0.f;

    SDL_WM_SetCaption("WNGF Demo", NULL);
}

void wngf_quit(struct WNGF_renderer *target)
{
    if(target == NULL)
        return;

    #ifdef WNGF_DEBUG
    uint_fast32_t total_cleaned=0, total_count=0;
    #endif

    for(uint_fast32_t i=0; i < target->buffers_matrix_count; i++)
    {
        free(target->buffers_matrix[i]);
    #ifdef WNGF_DEBUG
        total_cleaned += sizeof(struct WNGF_mat4x4);
        total_count++;
    #endif
    }
    free(target->buffers_matrix);
    target->buffers_matrix_count = 0;

    #ifdef WNGF_DEBUG
    printf("Freed %lu bytes of matrix buffers (%lu buffers).\n", total_cleaned, total_count);
    total_cleaned = 0;
    total_count = 0;
    #endif

    for(uint_fast32_t i=0; i < target->buffers_mesh_count; i++)
    {
        if(target->buffers_mesh[i] == NULL)
            continue;
        free(target->buffers_mesh[i]->tris);

    #ifdef WNGF_DEBUG
        total_cleaned += target->buffers_mesh[i]->tris_count * sizeof(struct WNGF_triangle);
        total_count++;
    #endif

        free(target->buffers_mesh[i]);
    }
    free(target->buffers_mesh);
    target->buffers_mesh_count = 0;

    #ifdef WNGF_DEBUG
    printf("Freed %lu bytes of mesh buffers (%lu buffers).\n", total_cleaned, total_count);
    total_cleaned = 0;
    total_count = 0;
    #endif

    for(uint_fast32_t i=0; i < target->buffers_triangle_count; i++)
    {
        free(target->buffers_triangle[i]);
    #ifdef WNGF_DEBUG
        total_cleaned += sizeof(struct WNGF_triangle);
        total_count++;
    #endif
    }
    free(target->buffers_triangle);
    target->buffers_triangle_count = 0;

    #ifdef WNGF_DEBUG
    printf("Freed %lu bytes of triangle buffers (%lu buffers).\n", total_cleaned, total_count);
    #endif

    SDL_FreeSurface(target->render_texture);

    free(target);
}

void wngf_set_triangle_verts(WNGF_triangle_ptr tri,
                             float x0,float y0,float z0,
                             float x1,float y1,float z1,
                             float x2,float y2,float z2)
{
    if(tri == NULL)
        return;

    tri->verts[0].x = x0; tri->verts[0].y = y0; tri->verts[0].z = z0;
    tri->verts[1].x = x1; tri->verts[1].y = y1; tri->verts[1].z = z1;
    tri->verts[2].x = x2; tri->verts[2].y = y2; tri->verts[2].z = z2;
}

void wngf_set_triangle_norms(WNGF_triangle_ptr tri, float nx, float ny, float nz)
{
    (void)tri; (void)nx; (void)ny; (void)nz;
//    if(tri == NULL)
//        return;

//    tri->normal.x = nx; tri->normal.y = ny; tri->normal.z = nz;
}

void wngf_matrix_mul_vec3(WNGF_mat4x4_ptr m, WNGF_vec3 *v1, WNGF_vec3 *out)
{
    if(m == NULL || v1 == NULL || out == NULL)
        return;

    out->x =    m->mx[0][0] * v1->x +
                m->mx[1][0] * v1->y +
                m->mx[2][0] * v1->z +
                m->mx[3][0];

    out->y =    m->mx[0][1] * v1->x +
                m->mx[1][1] * v1->y +
                m->mx[2][1] * v1->z +
                m->mx[3][1];

    out->z =    m->mx[0][2] * v1->x +
                m->mx[1][2] * v1->y +
                m->mx[2][2] * v1->z +
                m->mx[3][2];

    float _w =  m->mx[0][3] * v1->x +
                m->mx[1][3] * v1->y +
                m->mx[2][3] * v1->z +
                m->mx[3][3];

    if(_w != 0.f)
    {
        out->x /= _w;
        out->y /= _w;
        out->z /= _w;
    }
}

void wngf_drawline(struct WNGF_renderer *target, float x0, float y0, float x1, float y1)
{
    if(target == NULL)
        return;

    lineColor(	target->final_surf,
                (short)x0, (short)y0,
                (short)x1, (short)y1,
                target->render_color); // Draw line
}

void wngf_drawtri(struct WNGF_renderer *target, WNGF_triangle_ptr tri)
{
    if(target == NULL || tri == NULL)
        return;

    if((target->draw_mode & 1) == 1)
    {
        fprintf(stderr, "Incorrect DRAW_MODE %2X\n", target->draw_mode);
        return;
    }

    if(target->draw_mode == WNGF_DRAW_WIREFRAME)
        trigonColor(target->final_surf,
                    (short)tri->verts[0].x, (short)tri->verts[0].y,
                    (short)tri->verts[1].x, (short)tri->verts[1].y,
                    (short)tri->verts[2].x, (short)tri->verts[2].y,
                    target->render_color);
    else if(target->draw_mode == WNGF_DRAW_FILLED)
        filledTrigonColor(target->final_surf,
                          (short)tri->verts[0].x, (short)tri->verts[0].y,
                          (short)tri->verts[1].x, (short)tri->verts[1].y,
                          (short)tri->verts[2].x, (short)tri->verts[2].y,
                          target->render_color);
    else if(target->draw_mode == WNGF_DRAW_TEXTURED)
    {
        const short vx[3] = { (short)tri->verts[0].x,
                              (short)tri->verts[1].x,
                              (short)tri->verts[2].x }, // Verts(x)
                    vy[3] = { (short)tri->verts[0].y,
                              (short)tri->verts[1].y,
                              (short)tri->verts[2].y }; // Verts(y)
        texturedPolygon(target->final_surf,
                        vx, vy,
                        3,
                        target->render_texture,
                        0, 0);
    }
    else
        trigonColor(target->final_surf,
                      (short)tri->verts[0].x, (short)tri->verts[0].y,
                      (short)tri->verts[1].x, (short)tri->verts[1].y,
                      (short)tri->verts[2].x, (short)tri->verts[2].y,
                      target->render_color);
}

void wngf_drawmesh(struct WNGF_renderer *target, WNGF_mesh_ptr mesh,
                   float px, float py, float pz,
                   float rx, float ry, float rz,
                   WNGF_mat4x4_ptr mat_rot_x,
                   WNGF_mat4x4_ptr mat_rot_y,
                   WNGF_mat4x4_ptr mat_rot_z,
                   WNGF_triangle_ptr *tmp_tris)
{
    if(target == NULL) // Target must exist
        return;
    if(mesh == NULL) // Don't draw "NOTHING"
        return;
    if(tmp_tris == NULL)
        return;

    WNGF_triangle_ptr	tri_proj		= tmp_tris[0],
                        tri_translated	= tmp_tris[1], // Translation at 0
                        tri_rotated_z	= tmp_tris[2],
                        tri_rotated_zx	= tmp_tris[3];

#ifdef WNGF_ROTATIONS_DISABLED
    (void)rx; (void)ry; (void)rz;
    (void)mat_rot_x; (void)mat_rot_y; (void)mat_rot_z;
    (void)tri_rotated_z;
    (void)tri_rotated_zx;
#endif

    WNGF_vec3 campos; campos.x = 0.f; campos.y = 0.f; campos.z = 0.f;
    WNGF_vec3 normal, lineA, lineB;

    for(uint_fast32_t ti=0; ti < mesh->tris_count; ti++) // TriangleIterator = ti
    {
        for(uint_fast8_t vi=0; vi < 3; vi++) // VectorIterator = vi
        {
    #ifndef WNGF_ROTATIONS_DISABLED
            if(rx != 0.f)
                wngf_set_matrix_rx(mat_rot_x, rx);
            if(ry != 0.f)
                wngf_set_matrix_ry(mat_rot_y, rx);
            if(rz != 0.f)
                wngf_set_matrix_rz(mat_rot_z, rx);

            wngf_matrix_mul_vec3(mat_rot_z, &mesh->tris[ti].verts[vi],  &tri_rotated_z->verts[vi]);
            wngf_matrix_mul_vec3(mat_rot_x, &tri_rotated_z->verts[vi],  &tri_rotated_zx->verts[vi]);

            tri_translated->verts[vi].x = tri_rotated_zx->verts[vi].x + px;
            tri_translated->verts[vi].y = tri_rotated_zx->verts[vi].y - py; // invert py
            tri_translated->verts[vi].z = tri_rotated_zx->verts[vi].z + pz;
    #else
            memcpy(&tri_translated->verts[vi], &mesh->tris[ti].verts[vi], sizeof(WNGF_vec3));

            tri_translated->verts[vi].x += px;
            tri_translated->verts[vi].y -= py;
            tri_translated->verts[vi].z += pz;
    #endif
        }
        // Depth test
        lineA.x = tri_translated->verts[1].x - tri_translated->verts[0].x;
        lineA.y = tri_translated->verts[1].y - tri_translated->verts[0].y;
        lineA.z = tri_translated->verts[1].z - tri_translated->verts[0].z;

        lineB.x = tri_translated->verts[2].x - tri_translated->verts[0].x;
        lineB.y = tri_translated->verts[2].y - tri_translated->verts[0].y;
        lineB.z = tri_translated->verts[2].z - tri_translated->verts[0].z;

        normal.x = lineA.y * lineB.z - lineA.z * lineB.y;
        normal.y = lineA.z * lineB.x - lineA.x * lineB.z;
        normal.z = lineA.x * lineB.y - lineA.y * lineB.x;

        // This is not normal anymore
        normal.x *= (tri_translated->verts[0].x - campos.x);
        normal.y *= (tri_translated->verts[0].y - campos.y);
        normal.z *= (tri_translated->verts[0].z - campos.z);

        if( (normal.x + normal.y + normal.z) < 0.f)
        {
            for(uint_fast8_t vi=0; vi < 3; vi++)
            {
                wngf_matrix_mul_vec3(target->projection_matx, &tri_translated->verts[vi], &tri_proj->verts[vi]);

                // Translate (center)
                tri_proj->verts[vi].x += 1.f;
                tri_proj->verts[vi].y += 1.f;
                // Scale
                tri_proj->verts[vi].x *= .5f * (float)target->gf_w;
                tri_proj->verts[vi].y *= .5f * (float)target->gf_h;
            }

            wngf_drawtri(target, tri_proj);
        }
    }
}

WNGF_triangle_ptr wngf_create_buffer_triangle(struct WNGF_renderer *target)
{
    if(target == NULL)
        return NULL;

    WNGF_triangle_ptr newtri = (WNGF_triangle_ptr)malloc(sizeof(struct WNGF_triangle));
    if(newtri == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_TRI "\n");
        exit(WNGF_ALLOC_ERROR_ID);
    }
    memset(newtri, 0, sizeof(struct WNGF_triangle)); // Clear buffer

    target->buffers_triangle = realloc(target->buffers_triangle,
                                       (++target->buffers_triangle_count) *
                                        sizeof(WNGF_triangle_ptr*));
    if(target->buffers_triangle == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_PTR_TRI "\n");
        exit(WNGF_ALLOC_ERROR_ID);
    }
    target->buffers_triangle[target->buffers_triangle_count-1] = newtri;

    return newtri;
}

WNGF_mat4x4_ptr wngf_create_buffer_mat4x4(struct WNGF_renderer *target)
{
    if(target == NULL)
        return NULL;

    WNGF_mat4x4_ptr newmat4 = (WNGF_mat4x4_ptr)malloc(sizeof(struct WNGF_mat4x4));
    if(newmat4 == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_MAT4 "\n");
        return NULL;
    }
    memset(newmat4, 0, sizeof(struct WNGF_mat4x4));

    target->buffers_matrix = realloc(target->buffers_matrix,
                                     (++target->buffers_matrix_count) *
                                      sizeof(WNGF_mat4x4_ptr*));
    if(target->buffers_matrix == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_PTR_MAT4 "\n");
        exit(WNGF_ALLOC_ERROR_ID);
    }
    target->buffers_matrix[target->buffers_matrix_count-1] = newmat4;

    return newmat4;
}

WNGF_mesh_ptr wngf_create_buffer_mesh(struct WNGF_renderer *target, uint_fast32_t triangle_count)
{
    if(target == NULL)
        return NULL;

    WNGF_mesh_ptr newmesh = (WNGF_mesh_ptr)malloc(sizeof(struct WNGF_mesh));
    if(newmesh == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_MAT4 "\n");
        return NULL;
    }
    memset(newmesh, 0, sizeof(struct WNGF_mesh));
    //
    newmesh->tris = (WNGF_triangle_ptr)malloc(triangle_count * sizeof(struct WNGF_triangle));
    if(newmesh->tris == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_TRI "\n");
        exit(WNGF_ALLOC_ERROR_ID);
    }
    memset(newmesh->tris, 0, triangle_count*sizeof(struct WNGF_triangle));
    newmesh->tris_count = triangle_count;
    //

    target->buffers_mesh = realloc(target->buffers_mesh,
                                   (++target->buffers_mesh_count) *
                                    sizeof(WNGF_mesh_ptr*));
    if(target->buffers_mesh == NULL)
    {
        fprintf(stderr, WNGF_ALLOC_ERROR " " WNGF_BUFF_PTR_MESH "\n");
        exit(WNGF_ALLOC_ERROR_ID);
    }
    target->buffers_mesh[target->buffers_mesh_count-1] = newmesh;

    return newmesh;
}

void wngf_set_matrix_rx(WNGF_mat4x4_ptr m, float a)
{
    if(m == NULL)
        return;

    m->mx[0][0] = 1.f;
    m->mx[1][1] = +cosf(a * .5f);
    m->mx[1][2] = +sinf(a * .5f);
    m->mx[2][1] = -sinf(a * .5f);
    m->mx[2][2] = +cosf(a * .5f);
    m->mx[3][3] = 1.f;
}

void wngf_set_matrix_ry(WNGF_mat4x4_ptr m, float a) // For now, do nothing
{
    if(m == NULL)
        return;

    (void)a;
}

void wngf_set_matrix_rz(WNGF_mat4x4_ptr m, float a)
{
    if(m == NULL)
        return;

    m->mx[0][0] = +cosf(a);
    m->mx[0][1] = +sinf(a);
    m->mx[1][0] = -sinf(a);
    m->mx[1][1] = +cosf(a);
    m->mx[2][2] = 1.f;
    m->mx[3][3] = 1.f;
}

WNGF_mesh_ptr wngf_init_cube(struct WNGF_renderer *target, float a)
{
    // Creating centered cube

    float ph, nh;
    ph = a/2.f; // Positive Half
    nh = -ph;	// Negative Half

    WNGF_mesh_ptr out = wngf_create_buffer_mesh(target, 12); // 12 triangles
    // Front
    wngf_set_triangle_verts(&out->tris[0],
                            nh, nh, nh,
                            nh, ph, nh,
                            ph, ph, nh);
    wngf_set_triangle_verts(&out->tris[1],
                            nh, nh, nh,
                            ph, ph, nh,
                            ph, nh, nh);



    // Right
    wngf_set_triangle_verts(&out->tris[2],
                            ph, nh, nh,
                            ph, ph, nh,
                            ph, ph, ph);
    wngf_set_triangle_verts(&out->tris[3],
                            ph, nh, nh,
                            ph, ph, ph,
                            ph, nh, ph);

    // Back
    wngf_set_triangle_verts(&out->tris[4],
                            ph, nh, ph,
                            ph, ph, ph,
                            nh, ph, ph);
    wngf_set_triangle_verts(&out->tris[5],
                            ph, nh, ph,
                            nh, ph, ph,
                            nh, nh, ph);

    // Left
    wngf_set_triangle_verts(&out->tris[6],
                            nh, nh, ph,
                            nh, ph, ph,
                            nh, ph, nh);
    wngf_set_triangle_verts(&out->tris[7],
                            nh, nh, ph,
                            nh, ph, nh,
                            nh, nh, nh);

    // Top
    wngf_set_triangle_verts(&out->tris[8],
                            nh, ph, nh,
                            nh, ph, ph,
                            ph, ph, ph);
    wngf_set_triangle_verts(&out->tris[9],
                            nh, ph, nh,
                            ph, ph, ph,
                            ph, ph, nh);

    // Bottom
    wngf_set_triangle_verts(&out->tris[10],
                            ph, nh, ph,
                            nh, nh, ph,
                            nh, nh, nh);
    wngf_set_triangle_verts(&out->tris[11],
                            ph, nh, ph,
                            nh, nh, nh,
                            ph, nh, nh);
    return out;
}

void wngf_debug_mesh_tris(const WNGF_mesh_ptr mesh)
{
    uint_fast32_t tri_count = mesh->tris_count;
    for(size_t i=0; i < tri_count; i++)
    {
        const WNGF_triangle_ptr tri = &mesh->tris[i];
        for(size_t ti=0; ti < 3; ti++)
        {
            printf("(%f; %f; %f) ",
                    (double)tri->verts[ti].x,
                    (double)tri->verts[ti].y,
                    (double)tri->verts[ti].z);
        }
        putchar('\n');
    }
    printf("%lu triangles in total.\n", tri_count);
}


void wngf_normalize_vec3(WNGF_vec3 *vec)
{
    float len = sqrtf( vec->x * vec->x + vec->y * vec->y + vec->z * vec->z );
    if(len == 0.f)
        return;
    vec->x /= len;
    vec->y /= len;
    vec->z /= len;
}

void wngf_set_color(struct WNGF_renderer *target, uint8_t r, uint8_t g, uint8_t b)
{
    if(target == NULL)
        return;
    target->render_color = ((uint32_t)r << 24) | ((uint32_t) g << 16) | ((uint32_t)b << 8) | 0xff;
}

void wngf_clear_renderer(struct WNGF_renderer *target, uint32_t color)
{
    if(target == NULL)
        return;
    SDL_FillRect(target->final_surf, NULL, color);
}

void wngf_set_drawmode(struct WNGF_renderer *target, uint8_t mode)
{
    if(target == NULL)
        return;
    target->draw_mode = mode;
}

void wngf_drawtext(struct WNGF_renderer *target, short px, short py, const char *txt, uint32_t color)
{
    if(target == NULL)
        return;
    stringColor(target->final_surf, px, py, txt, (color<<8)|0xff);
}
