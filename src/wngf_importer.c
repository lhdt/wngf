#include "wngf_importer.h"

void wngf_load_obj(struct WNGF_renderer *target, const char *mdl_file, WNGF_mesh_ptr *out)
{
    if(target == NULL)
        return;

    FILE *md_fh = fopen(mdl_file, "r");
    if(md_fh == NULL) // Can't open file
        return;
    //
    WNGF_vec3 *vertcoords   = calloc(1, sizeof(WNGF_vec3)),
              *normalcoords = calloc(1, sizeof(WNGF_vec3));
    WNGF_vec2 *texcoords    = calloc(1, sizeof(WNGF_vec2));
    struct face_s {
        float verts[3][3]; // vertex
        float texcs[3][2]; // texcoord
        float norms[3][3]; // normalcoord
    };
    struct face_s *face_buff = malloc(sizeof(struct face_s));
    memset(face_buff, 0, sizeof(struct face_s));

    uint_fast64_t verts_count=0, norms_count=0, texcs_count=0, tri_count=0;
    //
    int sc_r=0;
    char *line_buff = calloc(128, 1);
    while(fgets(line_buff, 128, md_fh))
    {
#ifdef WNGF_DEBUG
        if(line_buff[0] == 'o')
        {
            char *fin_str = line_buff+2;
            fin_str[strlen(fin_str)-1] = '\0';
            printf("Loading model: '%s'\n", fin_str);
        }
        else
#endif
        if(line_buff[0] == 'v')
        {
            if(line_buff[1] == ' ') // Model coords
            {
                vertcoords = realloc(vertcoords, (verts_count+1)*sizeof(WNGF_vec3));
                if(vertcoords == NULL)
                    exit(-10); // ModelLoaderAllocError
                sc_r = sscanf(line_buff, "v %f %f %f",
                              &vertcoords[verts_count].x,
                              &vertcoords[verts_count].y,
                              &vertcoords[verts_count].z);
                if(sc_r != 3) // 3 vert coords
                {
                    fprintf(stderr, "Error in vertex parsing:\n%s\n", line_buff);
                    break;
                }
                verts_count++;
            }
            else if(line_buff[1] == 't') // Texture coords
            {
                texcoords = realloc(texcoords, (texcs_count+1)*sizeof(WNGF_vec3));
                if(texcoords == NULL)
                    exit(-10);
                sc_r = sscanf(line_buff, "vt %f %f",
                              &texcoords[texcs_count].x,
                              &texcoords[texcs_count].y);
                if(sc_r != 2) // 2 texture coords
                {
                    fprintf(stderr, "Error in vertex parsing:\n%s\n", line_buff);
                    break;
                }
                texcs_count++;
            }
            else if(line_buff[1] == 'n') // Normal coords
            {
                normalcoords = realloc(normalcoords, (norms_count+1)*sizeof(WNGF_vec3));
                if(normalcoords == NULL)
                    exit(-10);
                sc_r = sscanf(line_buff, "vn %f %f %f",
                              &normalcoords[norms_count].x,
                              &normalcoords[norms_count].y,
                              &normalcoords[norms_count].z);
                if(sc_r != 3) // 3 normal coords
                {
                    fprintf(stderr, "Error in vertex parsing:\n%s\n", line_buff);
                    break;
                }
                norms_count++;
            }
            else
            {
                fprintf(stderr, "Error in vertex parsing:\n%s\n", line_buff);
                break;
            }
        }
        else if(line_buff[0] == 'f')
        {
            long ids[9];
            uint_fast8_t with_texcs=1;
            sc_r = sscanf(line_buff, "f %lu/%lu/%lu %lu/%lu/%lu %lu/%lu/%lu",
                          &ids[0], &ids[1],
                          &ids[2], &ids[3],
                          &ids[4], &ids[5],
                          &ids[6], &ids[7],
                          &ids[8]);

            if(sc_r != 9) // 9 positions (v, vt, vn)*3
            {
                sc_r = sscanf(line_buff, "f %lu//%lu %lu//%lu %lu//%lu",
                              &ids[0], &ids[2], &ids[3],
                              &ids[5], &ids[6], &ids[8]);
                if(sc_r != 6)
                {
                    fprintf(stderr, "Error in faces parsing:\n%s\n", line_buff);
                    break;
                }
                with_texcs = 0;
            }
            //
            face_buff = realloc(face_buff, (tri_count+1)*sizeof(struct face_s));
            for(uint_fast8_t ti=0; ti < 3; ti++)
            {
                face_buff[tri_count].verts[ti][0] = vertcoords[ ids[3*ti]-1 ].x;
                face_buff[tri_count].verts[ti][1] = vertcoords[ ids[3*ti]-1 ].y;
                face_buff[tri_count].verts[ti][2] = vertcoords[ ids[3*ti]-1 ].z;

                if(with_texcs)
                {
                    face_buff[tri_count].texcs[ti][0] = texcoords[ ids[3*ti+1]-1 ].x;
                    face_buff[tri_count].texcs[ti][1] = texcoords[ ids[3*ti+1]-1 ].y;
                }

                face_buff[tri_count].norms[ti][0] = normalcoords[ ids[3*ti+2]-1 ].x;
                face_buff[tri_count].norms[ti][1] = normalcoords[ ids[3*ti+2]-1 ].y;
                face_buff[tri_count].norms[ti][2] = normalcoords[ ids[3*ti+2]-1 ].z;
            }
            //
            tri_count++;
        }
    }
    //
    *out = wngf_create_buffer_mesh(target, tri_count);
    for(uint_fast64_t ti=0; ti < tri_count; ti++)
    {
        for(uint_fast8_t vi=0; vi < 3; vi++)
        {
            (*out)->tris[ti].verts[vi].x     = face_buff[ti].verts[vi][0];
            (*out)->tris[ti].verts[vi].y     = face_buff[ti].verts[vi][1];
            (*out)->tris[ti].verts[vi].z     = face_buff[ti].verts[vi][2];

            (*out)->tris[ti].texcoords[vi].x = face_buff[ti].texcs[vi][0];
            (*out)->tris[ti].texcoords[vi].y = face_buff[ti].texcs[vi][1];

            (*out)->tris[ti].normals[vi].x   = face_buff[ti].norms[vi][0];
            (*out)->tris[ti].normals[vi].y   = face_buff[ti].norms[vi][1];
            (*out)->tris[ti].normals[vi].z   = face_buff[ti].norms[vi][2];
        }
    }
    //
    free(vertcoords);
    free(normalcoords);
    free(texcoords);
    free(line_buff);
    free(face_buff);
}
