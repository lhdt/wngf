TARGET=wingfull-demo

CFLAGS=-O2 -ggdb -Iinclude/ -Wall -Werror
LDFLAGS=-fPIC -lSDL -lSDL_image -lSDL_gfx -lm -lpthread

SRCFILES=$(wildcard src/*.c)
OBJFILES=$(SRCFILES:src/%.c=obj/%.o)

obj/%.o: src/%.c
	gcc $(CFLAGS) -c $< -o $@

all: $(OBJFILES)
	gcc $(CFLAGS) main.c -o $(TARGET) $(OBJFILES) $(LDFLAGS)

$(TARGET): all

run: $(TARGET)
	@./$(TARGET)

clean:
	@rm -f $(OBJFILES) $(TARGET)
