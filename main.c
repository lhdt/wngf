#include <stdio.h>
#include <locale.h>
#include <time.h>

#include <SDL/SDL_image.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_gfxPrimitives_font.h>

#include "include/wngf_core.h"
#include "include/wngf_importer.h"

//
#define ROT_DELTA (0.01f)
//

int main()
{
    setlocale(LC_ALL, "");
    setlocale(LC_NUMERIC, "C");

    int r = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTTHREAD);
    if(r != 0)
    {
        fprintf(stderr, "Failed to init SDL\nError message: %s\n", SDL_GetError());
        exit(-5);
    }
    IMG_Init(IMG_INIT_PNG | IMG_INIT_TIF);

    uint16_t scrw = 1920, scrh = 1080;

    srand(time(NULL));
    SDL_Surface *win_screen = SDL_SetVideoMode(scrw, scrh, 24, SDL_HWSURFACE);
    if(win_screen == NULL)
    {
        win_screen = SDL_SetVideoMode(scrw, scrh, 24, SDL_SWSURFACE);
        if(win_screen == NULL)
        {
            printf("Cannot create SDL surface!\n");
            exit(-1);
        }
        printf("Fallback to SW surface\n");
    }
    
    struct WNGF_renderer *inst;
    wngf_init_renderer(scrw, scrh, DEF_FOV, &inst);
    if(inst == NULL)
        return -3;
    inst->final_surf = win_screen;

    WNGF_mesh_ptr democube, demosphere, demomonkey, demofiat3000;
    wngf_load_obj(inst, "models/cube.obj", &democube);
    wngf_load_obj(inst, "models/sphere.obj", &demosphere);
    wngf_load_obj(inst, "models/suzanne.obj", &demomonkey);
    wngf_load_obj(inst, "models/fiat3000.obj", &demofiat3000);


    // Matrices
    WNGF_triangle_ptr *tmp_tri_arr = malloc(4*sizeof(WNGF_triangle_ptr));
    for(int i=0; i < 4; i++)
        tmp_tri_arr[i] = wngf_create_buffer_triangle(inst);

    WNGF_mat4x4_ptr mx_x = wngf_create_buffer_mat4x4(inst),
                    mx_y = wngf_create_buffer_mat4x4(inst),
                    mx_z = wngf_create_buffer_mat4x4(inst);
    memset(mx_x, 0, sizeof(struct WNGF_mat4x4));
    wngf_set_matrix_rx(mx_x, 0.f);
    memset(mx_y, 0, sizeof(struct WNGF_mat4x4));
    wngf_set_matrix_ry(mx_y, 0.f);
    memset(mx_z, 0, sizeof(struct WNGF_mat4x4));
    wngf_set_matrix_rz(mx_z, 0.f);
    //

    SDL_Event shev;
    uint8_t shq = 0;

#ifdef WNGF_COUNT_FPS
    time_t t_last=0, t_period=0;
    uint64_t t_counter=0;
    double t_fps_sum=0., _current_fps=0.;
    char *fpstext = calloc(16, 1);
#endif
    //
    float _rot_angle=0.f;
    while(!shq)
    {
        wngf_clear_renderer(inst, WNGF_BLACK);
#ifdef WNGF_COUNT_FPS
        t_last = SDL_GetTicks();

        sprintf(fpstext, "FPS: %.2f", _current_fps);
        wngf_drawtext(inst, 0, 0, fpstext, WNGF_GREEN);
#endif
        while(SDL_PollEvent(&shev))
        {
            if(shev.type == SDL_QUIT)
            {
                shq = 1;
                break;
            }
            if(shev.type == SDL_KEYDOWN)
            {
                const char *kname = SDL_GetKeyName(shev.key.keysym.sym);
                if(strcmp(kname, "q") == 0)
                {
                    shq = 1;
                    break;
                }
            }
        }
        wngf_set_drawmode(inst, WNGF_DRAW_WIREFRAME);

        wngf_drawmesh(inst, demosphere,
                      6.f, 0.f, 5.f,
                      _rot_angle, _rot_angle, _rot_angle,
                      mx_x, mx_y, mx_z, tmp_tri_arr
                      );

        wngf_drawmesh(inst, demomonkey,
                      -6.f, 0.f, 5.f,
                      _rot_angle, _rot_angle, _rot_angle,
                      mx_x, mx_y, mx_z, tmp_tri_arr
                      );

        wngf_drawmesh(inst, demofiat3000,
                      0.f, 0.f, 5.f,
                      _rot_angle, _rot_angle, _rot_angle,
                      mx_x, mx_y, mx_z, tmp_tri_arr
                      );

        //
        _rot_angle += ROT_DELTA;
        //
#ifdef WNGF_COUNT_FPS
        t_period = SDL_GetTicks();
#endif
        SDL_Flip(win_screen);
        #ifdef WNGF_COUNT_FPS
        t_period = SDL_GetTicks() - t_last;
        if(t_period > 0.f)
        {
            _current_fps = 1000./t_period;
            t_fps_sum += _current_fps;
            t_counter++;
        }
        SDL_Delay(15); // 60fps
#endif
    }
#ifdef WNGF_COUNT_FPS
    printf("Average pFPS = %.2f FPS\n", t_fps_sum/t_counter);
#endif

    free(fpstext);
    wngf_quit(inst);
    free(tmp_tri_arr);
    SDL_Quit();
    return 0;
}
